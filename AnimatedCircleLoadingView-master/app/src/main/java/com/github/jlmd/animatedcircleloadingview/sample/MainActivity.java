package com.github.jlmd.animatedcircleloadingview.sample;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.*;

import com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView;

import org.json.JSONArray;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;



public class MainActivity extends Activity {

  private AnimatedCircleLoadingView animatedCircleLoadingView;
  Button exit;
  Button open;
  AsyncHttpClient client;
  String url = "https://newhome.onlosant.com/hfdhfgdhdfg/gpio/17";



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    animatedCircleLoadingView = (AnimatedCircleLoadingView) findViewById(R.id.circle_loading_view);

    exit = (Button) findViewById(R.id.reset);
    open = (Button) findViewById(R.id.open);

    exit.setVisibility(View.GONE);


    open.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startLoading();
        open.setVisibility(View.GONE);

        client = new AsyncHttpClient();
        client.get(MainActivity.this, url, new AsyncHttpResponseHandler() {
          @Override
          public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

          }

          @Override
          public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

          }
        });
        showExitButton();
      }
    });

    exit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });

    startPercentMockThread();
  }

  private void startLoading() {
    animatedCircleLoadingView.startDeterminate();
  }

  // Show button Exit with delay
  public void showExitButton() {
    exit.postDelayed(new Runnable() {
      @Override
      public void run() {
        exit.setVisibility(View.VISIBLE);
      }
    }, 1500 * 5);
    resetLoading();
  }

  private void startPercentMockThread() {
    Runnable runnable = new Runnable() {
      @Override
      public void run() {
        try {
          Thread.sleep(2000);
          for (int i = 0; i <= 100; i++) {
            Thread.sleep(30);
            changePercent(i);
          }
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    };
    new Thread(runnable).start();
  }

  private void changePercent(final int percent) {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        animatedCircleLoadingView.setPercent(percent);
      }
    });
  }

  public void resetLoading() {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        animatedCircleLoadingView.resetLoading();
      }
    });
  }
}